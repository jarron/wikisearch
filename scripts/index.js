$(function() {
    var randomURL = "https://en.wikipedia.org/w/api.php?format=json&origin=*&action=query&list=random&rnnamespace=0";
    var searchURL = "https://en.wikipedia.org/w/api.php?origin=*&action=opensearch&search=";
    var articleURL = "https://en.wikipedia.org/wiki/";
    $("#search").on("keyup", function( event ) {
            var query = $("#search").val();
            if (query != "")
            {
            $.getJSON(searchURL + query, function (data) {
                $("#results").empty();
                for (let i = 0; i < data[1].length; i++)
                {
                    $("#results").append(
                          "<li class=\"result\">"
                        + "<a href=\"" + data[3][i] + "\" target=\"_blank\" rel=\"noopener noreferrer\">" + data[1][i] + "</a>"
                        + "<p>" + data[2][i] + "</p>"
                        + "</li>"
                        );
                }
            });
            }
            else
            {
                $("#results").empty();
            }
    });
    var title;
    $("#random").on("click", function() {   
        $("#search").val("");
        $.getJSON(randomURL, function (data) {
            $("#results").empty();
            title = data["query"]["random"][0]["title"];
                $("#results").append(
                      "<li class=\"result\">"
                    + "<a href=\"" + articleURL + title + "\" target=\"_blank\" rel=\"noopener noreferrer\">" + title + "</a>"
                    + "</li>"
                    );
            
        });
    });
});